<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login')->name('postlogin');

Route::group(['middleware' => 'auth'], function () {
 
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');

    Route::get('customer', 'login\Customer@index')->name('customer');
    Route::get('customer/profile/{id}', 'login\Customer@get')->name('getcustomer');
    Route::get('customer/new', 'login\Customer@new')->name('newcustomer');
    Route::post('customer/new', 'login\Customer@postnew')->name('postnewcustomer');
    Route::get('customer/edit/{id}', 'login\Customer@edit')->name('editcustomer');
	
    Route::get('product', 'login\Product@index')->name('product');
    Route::get('product/new', 'login\Product@new')->name('newproduct');
    Route::post('product/new', 'login\Product@postnew')->name('postnewproduct');
    Route::get('product/edit/{id}', 'login\Product@get')->name('getproduct');
    Route::post('product/edit', 'login\Product@put')->name('putproduct');

    Route::get('transaksi', 'login\Transaksi@index')->name('transaksi');
    Route::get('transaksi/view/{id}', 'login\Transaksi@get')->name('gettransaksi');
    Route::get('transaksi/new', 'login\Transaksi@new')->name('newtransaksi');
    Route::post('transaksi/new', 'login\Transaksi@postnew')->name('postnewtransaksi');
	Route::get('transaksi/print/{id}', 'login\Transaksi@print')->name('printtransaksi');
    Route::get('transaksi/pay/{id}/{mode}', 'login\Transaksi@pay')->name('pay');
    Route::get('transaksi/printpay/{id}', 'login\Transaksi@printpay')->name('printpay');

    Route::get('notification', 'login\Notification@index')->name('notification');
    Route::post('notification/send', 'login\Notification@postnotif')->name('postnotif');
    Route::post('notification/sendwa', 'login\Notification@postnotifwa')->name('postnotifwa');
});