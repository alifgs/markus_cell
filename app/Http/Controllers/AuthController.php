<?php
 
namespace App\Http\Controllers;

use App\model\tblpelanggan;
use Illuminate\Http\Request;
 
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash as FacadesHash;

class AuthController extends Controller
{
    public function showFormLogin()
    {
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            return redirect()->route('home');
        }
        return view('login');
    }
 
    public function login(Request $request)
    {
        //dd(FacadesHash::make($request->input('password')));
        $rules = [
            'email'                 => 'required|email',
            'password'              => 'required|string'
        ];
 
        $messages = [
            'email.required'        => 'Email wajib diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
 
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
 
        $data = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
 
        Auth::attempt($data);
 
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            Session::flash('type', 'success');
            Session::flash('message', 'Login success');
            return redirect()->route('home');
 
        } else { // false
            Session::flash('type', 'danger');
            Session::flash('message', 'Email atau password salah');
            return redirect()->route('login');
        }
 
    }
 
    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login');
    }
 
    public function logincustomer(Request $request, tblpelanggan $tblpelanggan){
        $data = $tblpelanggan->login($request->username, $request->pass);
        if(isset($data['idPelanggan'])){
            $data['token'] = md5('username:'.strtoupper($request->username).',pass:'.$request->pass);
            $tblpelanggan->updateIdDevice($data['idPelanggan'], $request->deviceid);
            return response()->json([
                'responsecode' => 201,
                'message'=>"Success",
                'data'=> $data
            ], 201);
        }
        return response()->json([
            'responsecode' => 200,
            'message'=>"Username atau Password Salah",
            'data'=> (object)[]
        ], 200);
    }
}