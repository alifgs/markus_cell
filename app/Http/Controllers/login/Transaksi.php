<?php
 
namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\model\tblpayment;
use App\model\tblpelanggan;
use App\model\tblproduct;
use App\model\tbltransaksi;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use App\customclass\helpers;
use Illuminate\Support\Facades\Session;

class Transaksi extends Controller
{
    public function index(tbltransaksi $tbltransaksi){
        //$data = $tbltransaksi->getAllData();
        $data = $tbltransaksi->getAllDataAndPelanggan();
        return view('login.transaksi.index',[
            'data'=>$data
        ]);
    }
    public function new(tblproduct $tblproduct, tblpelanggan $tblpelanggan){
        $pelanggan = $tblpelanggan->getAllData();
        $product = $tblproduct->getAllData();
        return view('login.transaksi.new',[
            'product'=>$product,
            'pelanggan'=>$pelanggan,
        ]
    );
    }

    public function postnew(Request $request, tblproduct $tblproduct, tblpelanggan $tblpelanggan, tbltransaksi $tbltransaksi, tblpayment $tblpayment){
        try {
            $denda = 10000;
            $newid = $tbltransaksi->newid();
            $kodetransaksi = "MKC".date("Ymd").($newid+1);
            $barang = array();
            for($i=1;$i<=$request->jmli;$i++){
                
                $imei = array();
                for($j=1;$j<=$request->jmlproduct.$i;$j++){
                    $newimei = [
                        'imei'.$j=>$request->{'imei'.$i.','.$j}
                    ];
                    array_push($imei,(object) $newimei);     
                }
                $newbarang = [
                    'product'=>$request->{'product'.$i},
                    'jmlproduct'=>$request->{'jmlproduct'.$i},
                    'modal'=>$request->{'modal'.$i},
                    'jual'=>$request->{'jual'.$i},
                    'imei'=>(object) $imei
                ];
                $check = $tblproduct->check($request->{'product'.$i},$request->{'modal'.$i},$request->{'jual'.$i});
                if(!$check==false){
                    $tblproduct->updatebarang($check->idProduct,$request->{'modal'.$i},$request->{'jual'.$i});
                }else{
                    $newproduct = [
                        'nama'=>$request->{'product'.$i},
                        'modal'=>$request->{'modal'.$i},
                        'jual'=>$request->{'jual'.$i}
                    ];
                    $tblproduct->addData($newproduct);
                }
                array_push($barang,$newbarang);
            }

            $arr = explode(" | NIK : ",$request->customer);
            $id = $tblpelanggan->getdatabyarr($arr);
            $data = [
                'idPelanggan'=>$id,
                'idBarang'=>json_encode((object)$barang),
                'kodetransaksi'=>$kodetransaksi,
                'total'=>$request->total,
                'modal'=>$request->totalmodal,
                'jual'=>$request->totaljual,
                'dp'=>$request->downpayment,
                'jangkawaktu'=>$request->payment,
                'cicilan'=>$request->perbulan,
                'komisi'=>$request->komisi,
                'komisiperbulan'=>$request->komisic,
                'jumlahdenda'=>$denda,
                'membername'=>$request->membername,
                'memberhp'=>$request->memberhp
            ];
            $idtransaksi = $tbltransaksi->addData($data);
            $cicilan = $request->payment;
            $tanggal = time();
            for($k=1;$k<=substr($cicilan, 1, strlen($cicilan));$k++){
                $tempo = strtotime('+30 day', $tanggal);
                $tanggal = $tempo;
                $datacicilan = [
                    'idTransaksi'=>$idtransaksi, 
                    'jumlah'=>strlen($cicilan),
                    'status'=>0,
                    'jatuhtempo'=>date('Y-m-d H:i:s', $tanggal),
                    'denda'=>$denda,
                    'metode'=>null,
                    'debitkredit'=>$request->perbulan,
                    'buktitransfer'=>null,
                ];
                $tblpayment->addData($datacicilan);
            }
            if($request->sales=="on"){
                $tblpelanggan->updatesales($request->customer);
            }
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
            return redirect()->route('printtransaksi',['id'=>$idtransaksi]);
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('transaksi');
    }

    public function gettrans(Request $request, tbltransaksi $tbltransaksi, tblpelanggan $tblpelanggan){
        $id = $tblpelanggan->checkmd5($request->header('token'));
        $data = $tbltransaksi->getDataById($id);
        if(isset($data)){
            return response()->json([
                'responsecode' => 200,
                'message'=>"Success",
                'data'=> $data
            ], 200);
        }
        return response()->json([
            'responsecode' => 200,
            'message'=>"Tidak ada Transaksi",
            'data'=> []
        ], 200);
    }

    public function print($id, tbltransaksi $tbltransaksi){
        $data = $tbltransaksi->getDataByIdprint($id);
        return view('login.transaksi.invoice',[
            'data'=>$data
        ]);
    }

    public function get($id, tbltransaksi $tbltransaksi, tblpayment $tblpayment){
        $data = $tbltransaksi->getDataByIdprint($id);
        $cicilan = $tblpayment->getTransById($data[0]->idTransaksi);
        return view('login.transaksi.get',[
            'data'=>$data,
            'cicilan'=>$cicilan
        ]);
    }

    public function pay($id, $mode, tblpayment $tblpayment){
        try{
            $kode = $mode=="pay"?2:4;
            $data = $tblpayment->updateStatus($id,$kode);
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('gettransaksi',['id'=>$data->idTransaksi]);
    }

    public function getpayment(Request $request, tblpayment $tblpayment, tblpelanggan $tblpelanggan){
        $id = $tblpelanggan->checkmd5($request->header('token'));
        if(!empty($id)){
            $data = $tblpayment->getTransById($request->idtrans);
            if(isset($data)){
                return response()->json([
                    'responsecode' => 200,
                    'message'=>"Success",
                    'data'=> $data
                ], 200);
            }
        }
        return response()->json([
            'responsecode' => 400,
            'message'=>"id tidak ditemukan",
            'data'=> []
        ], 200);
    }
    public function postpayment(Request $request, tblpayment $tblpayment, tblpelanggan $tblpelanggan){
        $id = $tblpelanggan->checkmd5($request->header('token'));
        if(!empty($id)){
            $uuid4          =Uuid::uuid4();
            $uuidfresh      =date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
        	$storagePath=Storage::disk('public')->putFileAs('/uploads/bukti/', $request->bukti, $uuidfresh.'.jpg');
        	$bukti=basename($storagePath);
            $data = $tblpayment->payment($request->idpayment,$bukti);
            if(isset($data)){
                return response()->json([
                    'responsecode' => 200,
                    'message'=>"Success",
                    'data'=> []
                ], 200);
            }else{
                return response()->json([
                    'responsecode' => 200,
                    'message'=>"Tidak ditemukan",
                    'data'=> []
                ], 200);
            }
        }
        return response()->json([
            'responsecode' => 400,
            'message'=>"id tidak ditemukan",
            'data'=> []
        ], 200);
    }
    
    public function printpay($id, tblpayment $tblpayment){
        $data = $tblpayment->getDataByIdprint($id);
        return view('login.transaksi.printpay',[
            'data'=>$data
        ]);
    }
}