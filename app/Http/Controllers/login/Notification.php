<?php
 
namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\model\tblnotifikasi;
use App\model\tblpelanggan;
use App\model\tblproduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;

class Notification extends Controller
{
    public function index(tblnotifikasi $tblnotifikasi, tblpelanggan $tblpelanggan){
        $data = $tblnotifikasi->getAllData();
        $pelanggan = $tblpelanggan->getAllData();
        return view('login.notification.index',[
            'data'=>$data,
            'pelanggan'=>$pelanggan
        ]);
    }
    public function postnotif(Request $request, tblnotifikasi $tblnotifikasi, tblpelanggan $tblpelanggan){
        try{
            $data=[
                'idPelanggan'=>$request->idPelanggan,
                'type'=>"Notification",
                'message'=>$request->message
            ];
            $pelanggan = $tblpelanggan->getDataById($request->idPelanggan);
            $this->sendNotificationFCM($pelanggan->idDevice,"Markus Cell",$request->message);
            $tblnotifikasi->addData($data);
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('notification');
    }

    public function postnotifwa(Request $request, tblnotifikasi $tblnotifikasi){
        try{
            if(substr($request->nohp,0,2)=="08"){
                $nohp = "62".substr($request->nohp,2,strlen($request->nohp));
            }else if(substr($request->nohp,0,1)=="+"){
                $nohp = substr($request->nohp,1,strlen($request->nohp));
            }
            $data=[
                'idPelanggan'=>$request->idPelanggan,
                'type'=>"Whatsapp",
                'message'=>$request->message
            ];
            $tblnotifikasi->addData($data);
            session()->flash('urlnya', 'https://api.whatsapp.com/send?phone='.$nohp.'&text='.$request->message);
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('notification');
    }

    function sendNotificationFCM($id, $title, $body) {
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key = AAAA8qcyaL8:APA91bGY5ZPPEnl8we3OywEEWiOzuD4PetI0QeZ63vTAlmltItlECCG5Iah-MaigS8XH88UhT5cbPKjNuVQHg7BuOFr5Sd8frBNHiec1fsalqERdDx7WlUr3_x4mWc_Uxl9raQpzGgAh'
        );
    
        $message = array(
            'to' => 'eQUxL108RQKQlGBiURZXGv:APA91bEBEKffk-5BRAkX2BFiZ1hZ_rQPwCiR9cla3zkoThlFmrFoUB6wV_VeUsH1MzNcCXuDfC8-2USWLzdzPAhFe1xAu9RChkUFfUCZb7I-G1g9MSZGQmZ_bsbC7vwsJGmZzNQ5SZ2W',
            'priority'=>'high',
            'notification' => array(
                "body" => $body,
                "title" => $title,
                "click_action"=> "SHOW_DETAILS"
            ),
            'data' => array(
                "body" => $body,
                "title" => $title,
                "id"=> $id,
                "click_action"=> "SHOW_DETAILS"
            ),
        );
    
        $ch = curl_init();
    
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($message)
        ));
    
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
}