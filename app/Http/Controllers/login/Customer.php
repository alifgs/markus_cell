<?php
 
namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\model\tblpelanggan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;

class Customer extends Controller
{
    public function index(tblpelanggan $tblpelanggan){
        $data = $tblpelanggan->getAllData();
        return view('login.customer.index',[
            'data'=>$data
        ]);
    }
    public function get($id, tblpelanggan $tblpelanggan){
        $data = $tblpelanggan->getDataById($id);
        return view('login.customer.get',[
            'd'=>$data
        ]);
    }
    public function new(){
        return view('login.customer.new');
    }
    public function postnew(Request $request, tblpelanggan $tblpelanggan){
        try{
            $uuid4          =Uuid::uuid4();
            $uuidfresh      =date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            if(isset($request->filektp)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filektp/', $request->filektp, $uuidfresh.'.'.$request->filektp->getClientOriginalExtension());
                $filektp=basename($storagePath);
            }
            if(isset($request->filekk)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filekk/', $request->filekk, $uuidfresh.'.'.$request->filekk->getClientOriginalExtension());
        	    $filekk=basename($storagePath);
            }
            if(isset($request->filenpwp)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filenpwp/', $request->filenpwp, $uuidfresh.'.'.$request->filenpwp->getClientOriginalExtension());
        	    $filenpwp=basename($storagePath);
            }
            if(isset($request->filebpjs)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filebpjs/', $request->filebpjs, $uuidfresh.'.'.$request->filebpjs->getClientOriginalExtension());
        	    $filebpjs=basename($storagePath);
            }
            if(isset($request->filesim)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filesim/', $request->filesim, $uuidfresh.'.'.$request->filesim->getClientOriginalExtension());
        	$filesim=basename($storagePath);
            }
            $pekerjaan = $request->pekerjaan=="Lainnya"?$request->Lainnya:$request->pekerjaan;
            $array = [];
            for($i=1;$i<=$request->jmlkontak;$i++){
                $datakontak = array(
                    'nama' => $request["kontaknama".$i], 
                    'hp' => $request["kontakhp".$i],
                    'hubungan' => $request["kontakhubungan".$i],
                );
                array_push($array, $datakontak);
            }
            $kontak = json_encode($array);
            $user = explode(" ", $request->nama);
            $data = [
                'username'=>$tblpelanggan->getUsername($user[0]), 
                'password'=>mt_rand(100000, 999999), 
                'nama'=>$request->nama, 
                'ttl'=>$request->ttl,
                'noktp'=>$request->noktp,
                'nonpwp'=>$request->nonpwp,
                'nokk'=>$request->nokk, 
                'nohp'=>$request->nohp, 
                'jk'=>$request->jk, 
                'statuspernikahan'=>$request->statuspernikahan,
                'tanggungan'=>$request->tanggungan,
                'alamat'=>$request->alamat,
                'statuskepemilikan'=>$request->statuskepemilikan, 
                'pekerjaan'=>$pekerjaan, 
                'statuspekerjaan'=>$request->statuspekerjaan,
                'jabatan'=>$request->jabatan,
                'namaperusahaan'=>$request->namaperusahaan,
                'alamatperusahaan'=>$request->alamatperusahaan,
                'notelkantor'=>$request->notelkantor, 
                'penghasilan'=>$request->penghasilan, 
                'waktupenghasilan'=>$request->waktupenghasilan, 
                'kontak'=>$kontak,
                'filektp'=>isset($filektp)?$filektp:null,
                'filekk'=>isset($filekk)?$filekk:null,
                'filenpwp'=>isset($filenpwp)?$filenpwp:null, 
                'filebpjs'=>isset($filebpjs)?$filebpjs:null, 
                'filesim'=>isset($filesim)?$filesim:null,
            ];
            $tblpelanggan->addData($data);
            return redirect()->route('customer');
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', $e);
        }
        return redirect()->route('customer');
    }
    public function edit($id, tblpelanggan $tblpelanggan){
        $data = $tblpelanggan->getDataById($id);
        return view('login.customer.edit',[
            'd'=>$data
        ]);
    }

    public function postedit(Request $request, tblpelanggan $tblpelanggan){
        try{
            $uuid4          =Uuid::uuid4();
            $uuidfresh      =date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            if(isset($request->filektp)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filektp/', $request->filektp, $uuidfresh.'.'.$request->filektp->getClientOriginalExtension());
                $filektp=basename($storagePath);
            }
            if(isset($request->filekk)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filekk/', $request->filekk, $uuidfresh.'.'.$request->filekk->getClientOriginalExtension());
        	    $filekk=basename($storagePath);
            }
            if(isset($request->filenpwp)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filenpwp/', $request->filenpwp, $uuidfresh.'.'.$request->filenpwp->getClientOriginalExtension());
        	    $filenpwp=basename($storagePath);
            }
            if(isset($request->filebpjs)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filebpjs/', $request->filebpjs, $uuidfresh.'.'.$request->filebpjs->getClientOriginalExtension());
        	    $filebpjs=basename($storagePath);
            }
            if(isset($request->filesim)){
                $storagePath=Storage::disk('public')->putFileAs('/uploads/filesim/', $request->filesim, $uuidfresh.'.'.$request->filesim->getClientOriginalExtension());
        	$filesim=basename($storagePath);
            }
            $pekerjaan = $request->pekerjaan=="Lainnya"?$request->Lainnya:$request->pekerjaan;
            $array = [];
            for($i=1;$i<=$request->jmlkontak;$i++){
                $datakontak = array(
                    'nama' => $request["kontaknama".$i], 
                    'hp' => $request["kontakhp".$i],
                    'hubungan' => $request["kontakhubungan".$i],
                );
                array_push($array, $datakontak);
            }
            $kontak = json_encode($array);
            $data = [
                'username'=>$request->username, 
                'password'=>$request->password, 
                'nama'=>$request->nama, 
                'ttl'=>$request->ttl,
                'noktp'=>$request->noktp,
                'nonpwp'=>$request->nonpwp,
                'nokk'=>$request->nokk, 
                'nohp'=>$request->nohp, 
                'jk'=>$request->jk, 
                'statuspernikahan'=>$request->statuspernikahan,
                'tanggungan'=>$request->tanggungan,
                'alamat'=>$request->alamat,
                'statuskepemilikan'=>$request->statuskepemilikan, 
                'pekerjaan'=>$pekerjaan, 
                'statuspekerjaan'=>$request->statuspekerjaan,
                'jabatan'=>$request->jabatan,
                'namaperusahaan'=>$request->namaperusahaan,
                'alamatperusahaan'=>$request->alamatperusahaan,
                'notelkantor'=>$request->notelkantor, 
                'penghasilan'=>$request->penghasilan, 
                'waktupenghasilan'=>$request->waktupenghasilan, 
                'kontak'=>$kontak,
            ];
            if(isset($filektp)){
                $data['filektp'] = $filektp;
            }
            if(isset($filekk)){
                $data['filekk'] = $filekk;
            }
            if(isset($filenpwp)){
                $data['filenpwp'] = $filenpwp;
            }
            if(isset($filebpjs)){
                $data['filebpjs'] = $filebpjs;
            }
            if(isset($filesim)){
                $data['filesim'] = $filesim;
            }
            
            $tblpelanggan->updateData($request->idPelanggan,$data);
            return redirect()->route('customer');
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', $e);
        }
        return redirect()->route('customer');
    }
}