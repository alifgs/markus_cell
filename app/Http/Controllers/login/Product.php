<?php
 
namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\model\tblpelanggan;
use App\model\tblproduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;

class Product extends Controller
{
    public function index(tblproduct $tblproduct){
        $data = $tblproduct->getAllData();
        return view('login.product.index',[
            'data'=>$data
        ]);
    }
    public function new(){
        return view('login.product.new');
    }

    public function postnew(Request $request, tblproduct $tblproduct){
        try {
            $data = [
                'nama'=>$request->nama,
                'modal'=>$request->modal,
                'jual'=>$request->jual
            ];
            $tblproduct->addData($data);
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('product');
    }
    
    public function get($id, tblproduct $tblproduct){
        return view('login.product.get', [
            'd'=>$tblproduct->getDataById($id)
        ]);
    }

    public function put(Request $request, tblproduct $tblproduct){
        try {
            $id = $request->id;
            $data = (object)[
                'nama'=>$request->nama,
                'modal'=>$request->modal,
                'jual'=>$request->jual
            ];
            $tblproduct->putDataById($id,$data);
            Session::flash('type', 'success');
            Session::flash('message', 'Proses Data success');
        }catch(\Exception $e){
            Session::flash('type', 'danger');
            Session::flash('message', 'Data tidak di temukan');
        }
        return redirect()->route('product');
    }
}