<?php
 
namespace App\Http\Controllers;

use App\model\tblpayment;
use App\model\tblpelanggan;
use App\model\tbltransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(tblpelanggan $tblpelanggan, tblpayment $tblpayment, tbltransaksi $tbltransaksi){
        
        $register = $tblpelanggan->register();
        $indebt = $tbltransaksi->indebt();
        $inoverdue = $tbltransaksi->overdue();
        $transaksi = $tbltransaksi->transaksi();
        $fund = $tblpayment->fund();
        $income = $tblpayment->income();
        $profit = $tblpayment->profit();
        return view('dashboard',[
            "register"=>$register,
            'indebt'=>$indebt,
            'jmlindebt'=>count($indebt),
            'inoverdue'=>$inoverdue,
            'jmlinoverdue'=>count($inoverdue),
            'transaksi'=>$transaksi,
            'fund'=>$fund,
            'income'=>$income,
            'profit'=>$profit

        ]);
    }
}