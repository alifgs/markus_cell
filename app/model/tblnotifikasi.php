<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class tblnotifikasi extends Model
{
    protected $table = "tblnotifikasi";
    protected $primaryKey = 'idNotif';
    protected $fillable = [
        'idNotif',
        'idPelanggan', 
        'type',
        'message',
    ]; 

    public function addData($data){
        $data=tblnotifikasi::newInstance($data);
        $data->save();
        return $data;
    }

    public function getAllData(){
        $data=tblnotifikasi::orderBy('tblnotifikasi.created_at', 'DESC')
        ->join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tblnotifikasi.idPelanggan')
        ->get();
        return $data;
    }
}