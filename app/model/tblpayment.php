<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class tblpayment extends Model
{
    protected $table = "tblpayment";
    protected $primaryKey = 'idPayment';
    protected $fillable = [
        'idPayment',
        'idTransaksi', 
        'jumlah',
        'status',
        'jatuhtempo',
        'denda',
        'metode',
        'debitkredit',
        'buktitransfer',
    ]; 

    public function addData($data){
        $data=tblpayment::newInstance($data);
        $data->save();
        return $data;
    }

    public function getTransById($id){
        $data=tblpayment::where('idTransaksi', $id)->get();
        return $data;
    }

    public function updateStatus($id, $kode){
        $data=tblpayment::where('idPayment', $id)->first();
        $data->status = $kode;
        $data->tanggalbayar=date('Y-m-d H:i:s');
        if(!isset($data->metode)){
            $data->metode = "Cash";
        }
        if($kode==4){
            $data->metode = "Gratis";
        }
        $data->save();
        return $data;
    }

    public function payment($id, $bukti){
        $data=tblpayment::where('idPayment', $id)->first();
        $data->status = 3;
        $data->tanggalbayar=date('Y-m-d H:i:s');
        $data->metode = "Transfer";
        $data->buktitransfer = $bukti;
        $data->save();
        return $data;
    }

    public static function check3(){
        $data=tblpayment::select('tblpelanggan.nama', 'tblpayment.buktitransfer', 'tblpayment.debitkredit','tblpayment.metode', 'tbltransaksi.kodetransaksi', 'tblpayment.tanggalbayar', 'tbltransaksi.idTransaksi')
        ->where('tblpayment.status', 3)
        ->join('tbltransaksi', 'tblpayment.idTransaksi', '=', 'tbltransaksi.idTransaksi')
        ->join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        return $data;
    }

    public static function telat(){
        //dd(Carbon::now());
        $data=tblpayment::select('tblpayment.jatuhtempo','tblpelanggan.nama', 'tblpayment.buktitransfer', 'tblpayment.debitkredit','tblpayment.metode', 'tbltransaksi.kodetransaksi', 'tblpayment.tanggalbayar', 'tbltransaksi.idTransaksi')
        ->whereDate('tblpayment.jatuhtempo', '<=', Carbon::now())
        ->join('tbltransaksi', 'tblpayment.idTransaksi', '=', 'tbltransaksi.idTransaksi')
        ->join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        return $data;
    }

    public static function warning(){
        //dd(Carbon::now());
        $data=tblpayment::select('tblpayment.jatuhtempo','tblpelanggan.nama', 'tblpayment.buktitransfer', 'tblpayment.debitkredit','tblpayment.metode', 'tbltransaksi.kodetransaksi', 'tblpayment.tanggalbayar', 'tbltransaksi.idTransaksi')
        ->whereBetween('tblpayment.jatuhtempo', [Carbon::now(),Date('y:m:d', strtotime('+5 days'))])
        ->join('tbltransaksi', 'tblpayment.idTransaksi', '=', 'tbltransaksi.idTransaksi')
        ->join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        return $data;
    }

    public static function getDataByIdprint($id){
        $data=tblpayment::where('tblpayment.idPayment', $id)
        ->join('tbltransaksi', 'tbltransaksi.idTransaksi', '=', 'tblpayment.idTransaksi')
        ->join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        return $data;
    }

    public function getcicilanke($id){
        $data=tblpayment::select('idPayment')
        ->where('idTransaksi', $id)
        ->orderBy('idPayment', 'ASC')
        ->get();
        return $data;
    }

    public static function indebt($id){
        $data=tblpayment::where('idTransaksi', $id)
        ->whereDate('tblpayment.jatuhtempo', '>', Carbon::now())
        ->where('status', 0)
        ->orderBy('idPayment')
        ->first();
        return $data;
    }

    public static function overdue($id){
        $data=tblpayment::where('idTransaksi', $id)
        ->whereDate('tblpayment.jatuhtempo', '<=', Carbon::now())
        ->where('status', 0)
        ->orderBy('idPayment')
        ->first();
        return $data;
    }

    public static function check($id){
        $data=tblpayment::where('idTransaksi', $id)
        ->where('status', 0)
        ->get()
        ->count();
        return $data>0?false:true;
    }

    public static function fund(){
        $data=tblpayment::
        where('status', 2)
        ->get();
        $jumlah = 0;
        foreach($data as $d){
            $jumlah+=$d->debitkredit;
        }
        return $jumlah;
    }
    public static function income(){
        $data=tbltransaksi::get();
        $jumlah = 0;
        foreach($data as $d){
            $jumlah+=$d->total;
        }
        return $jumlah;
    }
    public static function profit(){
        $data=tbltransaksi::get();
        $jumlah = 0;
        foreach($data as $d){
            if($d->jangkawaktu=='c3'){
                for($i=1;$i<=3;$i++){
                    $jumlah+=$d->jual - $d->modal;
                }
            }else if($d->jangkawaktu=='c6'){
                for($i=1;$i<=6;$i++){
                    $jumlah+=$d->jual - $d->modal;
                }
            }else if($d->jangkawaktu=='c9'){
                for($i=1;$i<=9;$i++){
                    $jumlah+=$d->jual - $d->modal;
                }
            }else if($d->jangkawaktu=='c12'){
                for($i=1;$i<=12;$i++){
                    $jumlah+=$d->jual - $d->modal;
                }
            }
        }
        return $jumlah;
    }
}