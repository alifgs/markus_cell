<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tbltransaksi extends Model
{
    protected $table = "tbltransaksi";
    protected $primaryKey = 'idTransaksi';
    protected $fillable = [
        'idTransaksi',
        'idPelanggan',
        'idBarang', 
        'kodetransaksi', 
        'harga', 
        'modal',
        'jual',
        'dp',
        'jangkawaktu', 
        'cicilan', 
        'komisi',
        'komisiperbulan',
        'jumlahdenda',
        'total',
        'membername',
        'memberhp'
    ];

    public function getAllData(){
        $data=tbltransaksi::orderBy('created_at', 'DESC')
        ->get();
        return $data;
    }
    public function getAllDataAndPelanggan(){
        $data=tbltransaksi::orderBy('tbltransaksi.created_at', 'DESC')
        ->join('tblpelanggan', 'tblpelanggan.idpelanggan', '=', 'tbltransaksi.idpelanggan')
        ->get();
        return $data;
    }
    public function getDataById($id){
        $data=tbltransaksi::where('idPelanggan', $id)->get()->toArray();
        return $data;
    }
    public function addData($data){
        
        $data=tbltransaksi::newInstance($data);
        $data->save();
        return $data->idTransaksi;
    }
    public function getUsername($username){
        $data=tbltransaksi::where('username', 'like', $username.'%')->get()->count();
        return $data==0?$username:($username.($data));
    }
    public function newid(){
        $data=tbltransaksi::select('idTransaksi')->orderBy('tbltransaksi.created_at', 'DESC')->first();
        if(isset($data->idTransaksi)){
            return $data->idTransaksi1;
        }else{
            return 1;
        }
    }

    public function getDataByIdprint($id){
        $data=tbltransaksi::where('tbltransaksi.idTransaksi', $id)
        ->join('tblpelanggan', 'tblpelanggan.idpelanggan', '=', 'tbltransaksi.idpelanggan')
        ->get();
        return $data;
    }

    public static function indebt(){
        $data=tbltransaksi::
        join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        $count = 0;
        $arr = [];
        foreach($data as $d){
            $check = tblpayment::indebt($d->idTransaksi);
            if(isset($check)){
                $count++;
                $orang = [
                    'nama'=>$d->nama,
                    'jatuhtempo'=>$check->jatuhtempo,
                    'debitkredit'=>$check->debitkredit,
                    'idTransaksi'=>$d->idTransaksi,
                    'kodetransaksi'=>$d->kodetransaksi,
                    'membername'=>$d->membername
                ];
                array_push($arr, $orang);
            }
        }
        return $arr;
    }

    public static function overdue(){
        $data=tbltransaksi::
        join('tblpelanggan', 'tblpelanggan.idPelanggan', '=', 'tbltransaksi.idPelanggan')
        ->get();
        $count = 0;
        $arr = [];
        foreach($data as $d){
            $check = tblpayment::overdue($d->idTransaksi);
            if(isset($check)){
                $count++;
                $orang = [
                    'nama'=>$d->nama,
                    'jatuhtempo'=>$check->jatuhtempo,
                    'debitkredit'=>$check->debitkredit,
                    'idTransaksi'=>$d->idTransaksi,
                    'kodetransaksi'=>$d->kodetransaksi,
                    'membername'=>$d->membername
                ];
                array_push($arr, $orang);
            }
        }
        return $arr;
    }
    public static function transaksi(){
        $data=tbltransaksi::get();
        $count = 0;
        foreach($data as $d){
            $check = tblpayment::check($d->idTransaksi);
            if($check){
                $count++;
            }
        }
        return $count;
    }
}