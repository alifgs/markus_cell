<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblpelanggan extends Model
{
    protected $table = "tblpelanggan";
    protected $primaryKey = 'idPelanggan';
    protected $fillable = [
        'idPelanggan',
        'username', 
        'password', 
        'nama', 
        'ttl',
        'noktp',
        'nonpwp',
        'nokk', 
        'nohp',
        'nowa', 
        'jk', 
        'statuspernikahan',
        'tanggungan',
        'alamat',
        'statuskepemilikan', 
        'pekerjaan', 
        'statuspekerjaan', 
        'jabatan',
        'namaperusahaan',
        'alamatperusahaan',
        'notelkantor', 
        'penghasilan', 
        'waktupenghasilan', 
        'kontak',
        'filektp',
        'filekk',
        'filenpwp', 
        'filebpjs', 
        'filesim', 
        'sales'
    ];

    public function getAllData(){
        $data=tblpelanggan::orderBy('created_at', 'DESC')->get();
        return $data;
    }
    public function getDataById($id){
        $data=tblpelanggan::where('idPelanggan', $id)->first();
        return $data;
    }
    public function addData($data){
        $data=tblpelanggan::newInstance($data);
        $data->save();
        return $data;
    }
    public function getUsername($username){
        $data=tblpelanggan::where('username', 'like', $username.'%')->get()->count();
        return $data==0?$username:($username.($data));
    }

    public function login($username, $password){
        try{
        $data=tblpelanggan::where('username', $username)->where('password', $password)->first();
        }catch(\exception $e){
            dd($e);
            return null;
        }
        return $data;
    }

    public function checkmd5($md5){
        $data=tblpelanggan::get();
        foreach($data as $d){
            $array = md5('username:'.strtoupper($d->username).',pass:'.$d->password);
            if($array==$md5){
                return $d->idPelanggan;
            }
        }
        return null;
    }

    public function updateIdDevice($id, $iddevice){
        $data = tblpelanggan::where('idPelanggan', $id)->first();
        $data->idDevice = $iddevice;
        $data->save();
        return $data;
    }
    public function register(){
        $data = tblpelanggan::get()->count();
        return $data;
    }

    public function updatesales($id){
        $arr = explode(" | NIK : ",$id);
        $data = tblpelanggan::where('nama', $arr[0])
        ->where('noktp', $arr[1])
        ->first();
        $data->sales = 1;
        $data->save();
        return $data;
    }

    public function getdatabyarr($arr){
        $data = tblpelanggan::where('nama', $arr[0])
        ->where('noktp', $arr[1])
        ->first();
        return $data->idPelanggan;
    }

    public function updateData($idPelanggan, $data){
        $data = tblpelanggan::where('idPelanggan', $idPelanggan)
        ->update($data);
        return $data;
    }
}