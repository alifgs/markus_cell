<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblproduct extends Model
{
    protected $table = "tblproduct";
    protected $primaryKey = 'idProduct';
    protected $fillable = [
        'nama',
        'modal', 
        'jual', 
    ];

    public function getAllData(){
        $data=tblproduct::orderBy('created_at', 'DESC')->get();
        return $data;
    }
    public function addData($data){
        $data=tblproduct::newInstance($data);
        $data->save();
        return $data;
    }
    public function getDataById($id){
        $data=tblproduct::where('idProduct', $id)->first();
        return $data;
    }
    public function putDataById($id, $update){
        $data=tblproduct::where('idProduct', $id)->first();
        $data->nama = $update->nama;
        $data->modal = $update->modal;
        $data->jual = $update->jual;
        $data->save();
        return $data;
    }
    public function check($nama){
        $data=tblproduct::where('nama', $nama)
        ->first();
        return $data;
    }
    public function updatebarang($id, $modal, $jual){
        $data=tblproduct::where('idProduct', $id)->first();
        $data->modal=$modal;
        $data->jual=$jual;
        $data->save();
        return $data;
    }
}