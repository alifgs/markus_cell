<?php
namespace App\customclass;

use App\model\tblpayment;
use App\model\tbltransaksi;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
class helpers
{
    static function status($id){
        $tblpayment = New tblpayment();
        $data = $tblpayment->getTransById($id);
        $lunas = 0;
        $count = 0;
        foreach($data as $d){
            if(!$d->status==0){
                $lunas++;
            }
            $count++;
        }
        if($lunas==$count&&$lunas>0){
            $return = "Lunas";
        }else{
            $return = $lunas."/".$count;
        }
        return $return;
    }

    static function check3(){
        $tblpayment = New tblpayment();
        $data = $tblpayment->check3();
        return $data;
    }
    static function telat(){
        $tblpayment = New tblpayment();
        $data = $tblpayment->telat();
        return $data;
    }
    static function warning(){
        $tblpayment = New tblpayment();
        $data = $tblpayment->warning();
        return $data;
    }

    static function notif(){
        $a1 = \App\customclass\helpers::check3();
        $a2 = \App\customclass\helpers::telat();
        $a3 = \App\customclass\helpers::warning();
        if($a1!='[]'||$a2!='[]'||$a3!='[]'){
            return 'true';
        }else{
            return 'false';
        }
    }

    static function humanTiming ($date)
    {
        $date1 = strtotime($date);
        $date2 = strtotime(date('Y-m-d H:i:s'));
        $seconds_diff = $date2 - $date1;

        echo round(abs($seconds_diff) / 60). " mins ago";

    }

    static function remaining($date){
        $future =  strtotime("now");
        $timefromdb = strtotime($date);
        $timeleft = $future-$timefromdb;
        $daysleft = round((($timeleft/24)/60)/60); 
        echo $daysleft." Hari";
    }
    static function lebih($date){
        $future =  strtotime($date);
        $timefromdb = strtotime("now");
        $timeleft = $future-$timefromdb;
        $daysleft = round((($timeleft/24)/60)/60); 
        echo $daysleft." Hari lagi";
    }

    static function cicilanke($idtrans, $idpayment){
        $tblpayment = New tblpayment();
        $data = $tblpayment->getcicilanke($idtrans);
        $first = 0;
        $count = 0;
        foreach($data as $d){
            $count++;
            if($count==1){
                $first = $d->idPayment;
            }
        }
        $ke = ($idpayment - $first)+1;
        return $ke.'/'.$count;
    }
}