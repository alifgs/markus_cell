@extends('layouts.app')

@section('content')
<div class="col-xl-3 col-md-12">
    <div class="card mat-stat-card">
        <div class="card-block">
            <div class="row align-items-center b-b-default">
                <div class="col-sm-6 b-r-default p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far fa-user text-c-purple f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>{{number_format($register)}}</h5>
                            <p class="text-muted m-b-0">Register</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="fas fa-calendar text-c-green f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>{{date('d m Y')}}</h5>
                            <p class="text-muted m-b-0">Date</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-sm-6 p-b-20 p-t-20 b-r-default">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far far fa-file-alt text-c-green f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>{{number_format($jmlindebt)}}</h5>
                            <p class="text-muted m-b-0">Debt</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far fa-bell text-c-red f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>{{number_format($jmlinoverdue)}}</h5>
                            <p class="text-muted m-b-0">Overdue</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-3 col-md-12">
    <div class="card mat-clr-stat-card text-white green ">
        <div class="card-block">
            <div class="row">
                <div class="col-3 text-center bg-c-green">
                    <i class="fas fa-trophy mat-icon f-24"></i>
                </div>
                <div class="col-9 cst-cont">
                    <h5>{{number_format($transaksi)}} Transaction</h5>
                    <p class="m-b-0">Sold</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card mat-clr-stat-card text-white blue">
        <div class="card-block">
            <div class="row">
                <div class="col-3 text-center bg-c-blue">
                    <i class="fas fa-star mat-icon f-24"></i>
                </div>
                <div class="col-9 cst-cont">
                    <h5>{{number_format($jmlindebt)}} Transaction</h5>
                    <p class="m-b-0">OnGoing</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xl-6 col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="card bg-c-green total-card">
                <div class="card-block">
                    <div class="text-left">
                        <h4>Rp. {{number_format($fund)}}</h4>
                        <p class="m-0">Fund</p>
                    </div>
                    {{-- <span class="label bg-c-red value-badges">15%</span> --}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card bg-c-blue total-card">
                <div class="card-block">
                    <div class="text-left">
                        <h4>Rp. {{number_format($profit)}}</h4>
                        <p class="m-0">Profit</p>
                    </div>
                    {{-- <span class="label bg-c-green value-badges">20%</span> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-c-yellow total-card">
                <div class="card-block">
                    <div class="text-center">
                        <h4>Rp. {{number_format($income)}}</h4>
                        <p class="m-0">Income</p>
                    </div>
                    {{-- <span class="label bg-c-red value-badges">15%</span> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Material statustic card end -->
<!-- order-visitor start -->


<!-- order-visitor end -->

<!--  sale analytics start -->
<div class="col-xl-6 col-md-12">
    <div class="card table-card">
        <div class="card-header">
            <h5>Customer in Debt</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                    <li><i class="fa fa-trash close-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover m-b-0 without-header">
                    <tbody>
                    @foreach($indebt as $d)
                        <tr>
                        
                            <td>
                                <div class="d-inline-block align-middle">
                                    <a href="{{route('gettransaksi',['id'=>$d['idTransaksi']])}}">
                                        <img src="assets/images/avatar-4.jpg" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        <div class="d-inline-block">
                                            <h6>{{$d['nama']}}</h6>
                                            <p>{{$d['membername']}}</p>
                                            <p>{{$d['kodetransaksi']}}<//p>
                                            <p class="text-muted m-b-0">{{$d['jatuhtempo']}}</p>
                                        </div>
                                    </a>
                                </div>
                            </td>
                            <td class="text-right">
                                <a href="{{route('gettransaksi',['id'=>$d['idTransaksi']])}}">
                                    <h6 class="f-w-700">Rp. {{number_format($d['debitkredit'])}}<i class="fas fa-level-up-alt text-c-green m-l-10"></i></h6>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div class="col-xl-6 col-md-12">
    <div class="card table-card">
        <div class="card-header">
            <h5>Customer in Overdue</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                    <li><i class="fa fa-trash close-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover m-b-0 without-header">
                    <tbody>
                        @foreach($inoverdue as $d)
                        <tr>
                            <td>
                                <div class="d-inline-block align-middle">
                                    <a href="{{route('gettransaksi',['id'=>$d['idTransaksi']])}}">
                                        <img src="assets/images/avatar-4.jpg" alt="user image" class="img-radius img-40 align-top m-r-15">
                                        <div class="d-inline-block">
                                            <h6>{{$d['nama']}}</h6>
                                            <p>{{$d['membername']}}</p>
                                            <p>{{$d['kodetransaksi']}}<//p>
                                            <p class="text-muted m-b-0">{{$d['jatuhtempo']}}</p>
                                        </div>
                                    </a>
                                </div>
                            </td>
                            <td class="text-right">
                                <a href="{{route('gettransaksi',['id'=>$d['idTransaksi']])}}">
                                    <h6 class="f-w-700">Rp. {{number_format($d['debitkredit'])}}<i class="fas fa-level-up-alt text-c-green m-l-10"></i></h6>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
@endsection
@section('js')
@endsection