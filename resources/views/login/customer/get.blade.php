@extends('layouts.app')

@section('content')
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Profile Customer</h5>
                </div>
                <div class="card-block">
                    <h6 >Bagian A.</h6>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Sesuai KTP</label>
                            <div class="col-sm-10">
                                <input name="nama" type="text" class="form-control" placeholder="Nama Sesuai KTP" value="{{$d->nama}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tempat / Tanggal Lahir</label>
                            <div class="col-sm-10">
                                <input name="ttl" type="text" class="form-control" placeholder="Tempat / Tanggal Lahir" value="{{$d->ttl}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. KTP</label>
                            <div class="col-sm-10">
                                <input name="noktp" type="number" class="form-control" placeholder="No. KTP" value="{{$d->noktp}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. NPWP</label>
                            <div class="col-sm-10">
                                <input name="nonpwp" type="text" class="form-control" placeholder="No. NPWP" value="{{$d->nonpwp}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Kartu Keluarga</label>
                            <div class="col-sm-10">
                                <input name="nokk" type="number" class="form-control" placeholder="No. Kartu Keluarga" value="{{$d->nokk}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Handphone</label>
                            <div class="col-sm-10">
                                <input name="nohp" type="text" class="form-control" placeholder="No. Handphone" value="{{$d->nohp}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Whatsapp</label>
                            <div class="col-sm-10">
                                <input name="nohp" type="text" class="form-control" placeholder="No. Whatsapp" value="{{$d->nowa}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-10">
                            <input name="jk" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->jk}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Pernikahan</label>
                            <div class="col-sm-10">
                            <input name="statuspernikahan" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->statuspernikahan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tanggungan</label>
                            <div class="col-sm-10">
                            <input name="tanggungan" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->tanggungan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat Tinggal</label>
                            <div class="col-sm-10">
                                <textarea name="alamat" rows="5" cols="5" class="form-control" placeholder="Alamat Tinggal" readonly>{{$d->alamat}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Kepemilikan</label>
                            <div class="col-sm-10">
                            <input name="statuskepemilikan" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->statuskepemilikan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jenis Pekerjaan</label>
                            <div class="col-sm-10">
                            <input name="pekerjaan" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->pekerjaan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Pekerjaan</label>
                            <div class="col-sm-10">
                            <input name="statuspekerjaan" type="text" class="form-control" placeholder="No. Handphone WA" value="{{$d->statuspekerjaan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jabatan & Bagian</label>
                            <div class="col-sm-10">
                                <input name="jabatan" type="text" class="form-control" placeholder="Jabatan & Bagian" value="{{$d->jabatan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Perusahaan</label>
                            <div class="col-sm-10">
                                <input name="namaperusahaan" type="text" class="form-control" placeholder="Nama Perusahaan" value="{{$d->namaperusahaan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat Perusahaan</label>
                            <div class="col-sm-10">
                                <textarea name="alamatperusahaan" rows="5" cols="5" class="form-control" placeholder="Alamat Perusahaan" readonly>{{$d->alamatperusahaan}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Telepon Kantor</label>
                            <div class="col-sm-10">
                                <input name="notelkantor" type="text" class="form-control" placeholder="Nomor Telepon Kantor" value="{{$d->notelkantor}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Penghasilan</label>
                            <div class="col-sm-10">
                                <input name="penghasilan" type="text" class="form-control" placeholder="Penghasilan" value="Rp. {{number_format($d->penghasilan,2)}} / {{$d->waktupenghasilan}}" readonly>
                            </div>
                        </div>
                            <h6 >KONTAK YANG BISA DIHUBUNGI</h6>
                            @php($kontak = json_decode($d->kontak))
                            @foreach($kontak as $k)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input name="nama" type="text" class="form-control" placeholder="Nama" value="{{$k->nama}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor Telepon / HP / Kantor</label>
                                    <div class="col-sm-10">
                                        <input name="nama" type="text" class="form-control" placeholder="Nomor Telepon / HP / Kantor" value="{{$k->hp}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Hubungan</label>
                                    <div class="col-sm-10">
                                        <input name="nama" type="text" class="form-control" placeholder="Hubungan" value="{{$k->hubungan}}" readonly>
                                    </div>
                                </div>
                            @endforeach    
                                <h6 >Checklist Kelengkapan Dokumen</h6>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">KTP Asli</label>
                                    <div class="col-sm-10">
                                        <a href="{{url('uploads/filektp/'.$d->filektp)}}" target="_blank">
                                        <img src="{{url('uploads/filektp/'.$d->filektp)}}" alt="" height="100"></a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kartu Keluarga (KK) Asli</label>
                                    <div class="col-sm-10">
                                        <a href="{{url('uploads/filekk/'.$d->filekk)}}" target="_blank">
                                        <img src="{{url('uploads/filekk/'.$d->filekk)}}" alt="" height="100"></a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">NPWP</label>
                                    <div class="col-sm-10">
                                        <a href="{{url('uploads/filenpwp/'.$d->filenpwp)}}" target="_blank">
                                        <img src="{{url('uploads/filenpwp/'.$d->filenpwp)}}" alt="" height="100"></a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">BPJS Kesehatan/Ketenagakerjaan</label>
                                    <div class="col-sm-10">
                                        <a href="{{url('uploads/filebpjs/'.$d->filebpjs)}}" target="_blank">
                                        <img src="{{url('uploads/filebpjs/'.$d->filebpjs)}}" alt="" height="100"></a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">SIM A / C</label>
                                    <div class="col-sm-10">
                                        <a href="{{url('uploads/filesim/'.$d->filesim)}}" target="_blank">
                                        <img src="{{url('uploads/filesim/'.$d->filesim)}}" alt="" height="100"></a>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <a href="{{Route('customer')}}" type="button" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Back</a>
                                </div>
                            
                        </div>
                </div>
            </div>
            <!-- Basic Form Inputs card end -->
        </div>
    </div>
@endsection

@section('css')
@endsection
@section('js')
<script type='text/javascript'>
        function addkontak(){
            // Number of inputs to create
            var number = document.getElementById("jmlkontak").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            for (i=1;i<=number;i++){
                
                var div1 = document.createElement('div');
                div1.classList.add("form-group");
                div1.classList.add("row");
                container.appendChild(div1);

                var label1 = document.createElement('label');
                label1.classList.add("col-sm-2");
                label1.classList.add("col-form-label");
                label1.style.fontWeight = "bold";
                label1.innerHTML = i+'). Nama';
                div1.appendChild(label1);

                var col1 = document.createElement('div');
                col1.classList.add("col-sm-10");
                div1.appendChild(col1);

                var input1 = document.createElement("input");
                input1.type = "text";
                input1.name = "kontaknama" + i;
                input1.classList.add("form-control");
                input1.placeholder = "Nama";
                col1.appendChild(input1);

                //nomer
                 var div2 = document.createElement('div');
                div2.classList.add("form-group");
                div2.classList.add("row");
                container.appendChild(div2);

                var label2 = document.createElement('label');
                label2.classList.add("col-sm-2");
                label2.classList.add("col-form-label");
                label2.innerHTML = 'Nomor Telepon / HP / Kantor';
                div2.appendChild(label2);

                var col2 = document.createElement('div');
                col2.classList.add("col-sm-10");
                div2.appendChild(col2);

                var input2 = document.createElement("input");
                input2.type = "text";
                input2.name = "kontakhp" + i;
                input2.classList.add("form-control");
                input2.placeholder = "Nomor Telepon / HP / Kantor";
                col2.appendChild(input2);
                
                //hubungan
                 var div3 = document.createElement('div');
                div3.classList.add("form-group");
                div3.classList.add("row");
                container.appendChild(div3);

                var label3 = document.createElement('label');
                label3.classList.add("col-sm-2");
                label3.classList.add("col-form-label");
                label3.innerHTML = 'Hubungan';
                div3.appendChild(label3);

                var col3 = document.createElement('div');
                col3.classList.add("col-sm-10");
                div3.appendChild(col3);

                var input3 = document.createElement("input");
                input3.type = "text";
                input3.name = "kontakhubungan" + i;
                input3.classList.add("form-control");
                input3.placeholder = "Hubungan";
                col3.appendChild(input3);
            }
        }
    </script>
@endsection