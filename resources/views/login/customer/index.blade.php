@extends('layouts.app')

@section('content')
<div class="col-md-2">
    <a class="btn fa fa-plus-circle btn-primary btn-md btn-block waves-effect waves-light text-center m-b-10" href="{{route('newcustomer')}}"><b> Tambah</b></a>
</div>
<div class="col-xl-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Data Customer</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table id="table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Username</th>
                            <th>Password</th>
                            <th style="white-space:nowrap">Nama</th>
                            <th>No.HP</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data as $d)
                        <tr>
                            <th><center>{{$i++}}</center></th>
                            <td>{{$d->username}}</td>
                            <td>{{$d->password}}</td>
                            <td>{{$d->nama}}</td>
                            <td>{{$d->nohp}}</td>
                            <td style="white-space:nowrap">{{$d->created_at}}</td>
                            <td><a href="{{route('getcustomer',['id'=>$d->idPelanggan])}}">View</a> | <a href="{{route('editcustomer',['id'=>$d->idPelanggan])}}">Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@endsection
@section('js')
@endsection