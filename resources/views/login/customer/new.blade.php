@extends('layouts.app')

@section('content')
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>New Customer</h5>
                </div>
                <div class="card-block">
                    <h6 >Bagian A.</h6>
                    <form action="{{ route('postnewcustomer') }}" method="post" class="md-float-material form-material" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Sesuai KTP</label>
                            <div class="col-sm-10">
                                <input name="nama" type="text" class="form-control" placeholder="Nama Sesuai KTP" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tempat / Tanggal Lahir</label>
                            <div class="col-sm-10">
                                <input name="ttl" type="text" class="form-control" placeholder="Tempat / Tanggal Lahir">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. KTP</label>
                            <div class="col-sm-10">
                                <input name="noktp" type="number" class="form-control" placeholder="No. KTP">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. NPWP</label>
                            <div class="col-sm-10">
                                <input name="nonpwp" type="text" class="form-control" placeholder="No. NPWP">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Kartu Keluarga</label>
                            <div class="col-sm-10">
                                <input name="nokk" type="number" class="form-control" placeholder="No. Kartu Keluarga">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Handphone</label>
                            <div class="col-sm-10">
                                <input name="nohp" type="text" class="form-control" placeholder="No. Handphone" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. Whatsapp</label>
                            <div class="col-sm-10">
                                <input name="nowa" type="text" class="form-control" placeholder="No. Whatsapp">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-10">
                                <select name="jk" class="form-control">
                                    <option disabled selected>Jenis Kelamin</option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Pernikahan</label>
                            <div class="col-sm-10">
                                <select name="statuspernikahan" class="form-control">
                                    <option disabled selected>Status Pernikahan</option>
                                    <option value="Lajang">Lajang</option>
                                    <option value="Menikah">Menikah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tanggungan</label>
                            <div class="col-sm-10">
                                <select name="tanggungan" class="form-control">
                                    <option disabled selected>Tanggungan</option>
                                    <option value="Tidak Ada">Tidak Ada</option>
                                    <option value="1 - 2 anak">1 - 2 anak</option>
                                    <option value="3 - 4 anak">3 - 4 anak</option>
                                    <option value=">5 anak">>5 anak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat Tinggal</label>
                            <div class="col-sm-10">
                                <textarea name="alamat" rows="5" cols="5" class="form-control" placeholder="Alamat Tinggal"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Kepemilikan</label>
                            <div class="col-sm-10">
                                <select name="statuskepemilikan" class="form-control">
                                    <option disabled selected>Status Kepemilikan</option>
                                    <option value="Sewa">Sewa</option>
                                    <option value="Kost">Kost</option>
                                    <option value="Milik Sendiri">Milik Sendiri</option>
                                    <option value="Milik Keluarga">Milik Keluarga</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jenis Pekerjaan</label>
                            <div class="col-sm-5">
                                <select name="pekerjaan" class="form-control">
                                    <option disabled selected>Jenis Pekerjaan</option>
                                    <option value="TNI/Polri">TNI/Polri</option>
                                    <option value="Freelancer">Freelancer</option>
                                    <option value="Pedagang">Pedagang</option>
                                    <option value="Pegawai Swasta">Pegawai Swasta</option>
                                    <option value="Pegawai Negeri">Pegawai Negeri</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input name="Lainnya" type="text" class="form-control" placeholder="Lainnya">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Status Pekerjaan</label>
                            <div class="col-sm-10">
                                <select name="statuspekerjaan" class="form-control">
                                    <option disabled selected>Status Pekerjaan</option>
                                    <option value="Tetap">Tetap</option>
                                    <option value="Kontrak">Kontrak</option>
                                    <option value="Paruh Waktu">Paruh Waktu</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jabatan & Bagian</label>
                            <div class="col-sm-10">
                                <input name="jabatan" type="text" class="form-control" placeholder="Jabatan & Bagian">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Perusahaan</label>
                            <div class="col-sm-10">
                                <input name="namaperusahaan" type="text" class="form-control" placeholder="Nama Perusahaan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat Perusahaan</label>
                            <div class="col-sm-10">
                                <textarea name="alamatperusahaan" rows="5" cols="5" class="form-control" placeholder="Alamat Perusahaan"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Telepon Kantor</label>
                            <div class="col-sm-10">
                                <input name="notelkantor" type="text" class="form-control" placeholder="Nomor Telepon Kantor">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Penghasilan</label>
                            <div class="col-sm-5">
                                <input name="penghasilan" type="number" class="form-control" placeholder="Penghasilan">
                            </div>
                            <div class="col-sm-5">
                                <select name="waktupenghasilan" class="form-control">
                                    <option disabled selected>Penghasilan</option>
                                    <option value="Bulanan">Bulan</option>
                                    <option value="Mingguan">Minggu</option>
                                    <option value="Harian">Hari</option>
                                </select>
                            </div>
                        </div>
                            <h6 >KONTAK YANG BISA DIHUBUNGI</h6>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jumlah Kontak</label>
                                    <div class="col-sm-2">
                                        <input id="jmlkontak" name="jmlkontak" type="text" class="form-control" placeholder="Jumlah Kontak" required>
                                    </div>
                                </div>
                                <div id="container"></div>
                                
                                <h6 >Checklist Kelengkapan Dokumen</h6>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">KTP Asli</label>
                                    <div class="col-sm-10">
                                        <input name="filektp" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kartu Keluarga (KK) Asli</label>
                                    <div class="col-sm-10">
                                        <input name="filekk" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">NPWP</label>
                                    <div class="col-sm-10">
                                        <input name="filenpwp" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">BPJS Kesehatan/Ketenagakerjaan</label>
                                    <div class="col-sm-10">
                                        <input name="filebpjs" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">SIM A / C</label>
                                    <div class="col-sm-10">
                                        <input name="filesim" type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Form Inputs card end -->
        </div>
    </div>
@endsection

@section('css')
@endsection
@section('js')
<script type='text/javascript'>
        $('#jmlkontak').on('input', function addkontak(){
            // Number of inputs to create
            var number = document.getElementById("jmlkontak").value;
            // Container <div> where dynamic content will be placed
            var container = document.getElementById("container");
            // Clear previous contents of the container
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            for (i=1;i<=number;i++){
                
                var div1 = document.createElement('div');
                div1.classList.add("form-group");
                div1.classList.add("row");
                container.appendChild(div1);

                var label1 = document.createElement('label');
                label1.classList.add("col-sm-2");
                label1.classList.add("col-form-label");
                label1.style.fontWeight = "bold";
                label1.innerHTML = i+'). Nama';
                div1.appendChild(label1);

                var col1 = document.createElement('div');
                col1.classList.add("col-sm-10");
                div1.appendChild(col1);

                var input1 = document.createElement("input");
                input1.type = "text";
                input1.name = "kontaknama" + i;
                input1.classList.add("form-control");
                input1.placeholder = "Nama";
                col1.appendChild(input1);

                //nomer
                 var div2 = document.createElement('div');
                div2.classList.add("form-group");
                div2.classList.add("row");
                container.appendChild(div2);

                var label2 = document.createElement('label');
                label2.classList.add("col-sm-2");
                label2.classList.add("col-form-label");
                label2.innerHTML = 'Nomor Telepon / HP / Kantor';
                div2.appendChild(label2);

                var col2 = document.createElement('div');
                col2.classList.add("col-sm-10");
                div2.appendChild(col2);

                var input2 = document.createElement("input");
                input2.type = "text";
                input2.name = "kontakhp" + i;
                input2.classList.add("form-control");
                input2.placeholder = "Nomor Telepon / HP / Kantor";
                col2.appendChild(input2);
                
                //hubungan
                 var div3 = document.createElement('div');
                div3.classList.add("form-group");
                div3.classList.add("row");
                container.appendChild(div3);

                var label3 = document.createElement('label');
                label3.classList.add("col-sm-2");
                label3.classList.add("col-form-label");
                label3.innerHTML = 'Hubungan';
                div3.appendChild(label3);

                var col3 = document.createElement('div');
                col3.classList.add("col-sm-10");
                div3.appendChild(col3);

                var input3 = document.createElement("input");
                input3.type = "text";
                input3.name = "kontakhubungan" + i;
                input3.classList.add("form-control");
                input3.placeholder = "Hubungan";
                col3.appendChild(input3);
            }
        }
    );
    </script>
@endsection