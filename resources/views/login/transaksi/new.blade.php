@extends('layouts.app')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>New Transaksi </h5>
                </div>
                <div class="card-block">
                    <form action="{{ route('postnewtransaksi') }}" method="post" class="md-float-material form-material" enctype="multipart/form-data">
                        @csrf
                        <input id="jmli" name="jmli" value="0" type="number" hidden>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Customer</label>
                            <div class="col-sm-5">
                                <input id="cust-id" list="customer" name="customer" class="form-control" required autocomplete="off">
                                <script>         
                                    document.getElementById('cust-id').addEventListener('input', function () {
                                        var x = document.getElementById("cust-id").value;
                                        var data = {!! json_encode($pelanggan->toArray()) !!}
                                        data.forEach(f1)
                                        function f1(item) {
                                            if((item['nama']+" | NIK : "+item['noktp'])==x){
                                                if(item['sales']==1){
                                                    $("#sales").prop("checked", true);
                                                }else{
                                                    $("#sales").prop("checked", false);
                                                }
                                                    var komisi = document.getElementById("komisi");
                                                    var lkomisi = document.getElementById("lkomisi");
                                                    var komisic = document.getElementById("komisic");
                                                    var lkomisic = document.getElementById("lkomisic");
                                                    var sales = document.getElementById("sales");
                                                    if(sales.checked==true){
                                                        komisi.value = 200000;
                                                        komisi.style.display = "block";
                                                        lkomisi.style.display = "block";
                                                        komisic.value = 100000;
                                                        komisic.style.display = "block";
                                                        lkomisic.style.display = "block";
                                                        lmembername.style.display = "block";
                                                        membername.style.display = "block";
                                                        lmemberhp.style.display = "block";
                                                        memberhp.style.display = "block";
                                                    }else{
                                                        komisi.value = '';
                                                        komisi.style.display = "none";
                                                        lkomisi.style.display = "none";
                                                        komisic.value = '';
                                                        komisic.style.display = "none";
                                                        lkomisic.style.display = "none";
                                                        membername.value = '';
                                                        lmembername.style.display = "none";
                                                        membername.style.display = "none";
                                                        memberhp.value = '';
                                                        lmemberhp.style.display = "none";
                                                        memberhp.style.display = "none";
                                                    }
                                                
                                            }
                                        }
                                    });
                                </script>
                                <datalist id="customer">
                                    @foreach($pelanggan as $p)
                                        <option id="{{$p->idPelanggan}}" value="{{$p->nama}} | NIK : {{$p->noktp}}">
                                    @endforeach
                                    </select>
                                </datalist>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sales</label>
                            <div class="col-sm-1">
                                <label class="switch">
                                <input type="checkbox" id="sales" name="sales">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" style="display:none" id="lkomisi">Komisi DP</label>
                            <div class="col-sm-2">
                                <input id="komisi" name="komisi" style="display:none" class="form-control"placeholder="Komisi">
                            </div>
                            <label class="col-sm-2 col-form-label text-center" style="display:none" id="lkomisic">Komisi Cicilan</label>
                            <div class="col-sm-2">
                                <input id="komisic" name="komisic" style="display:none" class="form-control"placeholder="Komisi Cicilan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" style="display:none"  id="lmembername">Member</label>
                            <div class="col-sm-2">
                                <input id="membername" name="membername" style="display:none" class="form-control"placeholder="Nama">
                            </div>
                            <label class="col-sm-2 col-form-label text-center" style="display:none" id="lmemberhp">No Hp</label>
                            <div class="col-sm-2">
                                <input id="memberhp" name="memberhp" style="display:none" class="form-control"placeholder="No Hp">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tambah Product</label>
                            <div class="col-sm-2">
                                <button type="button" id="btntambah" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Tambah +</button>
                            </div>
                        </div>
                        <div id="container"></div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Total Modal</label>
                            <div class="col-sm-3">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" placeholder="Total Modal" readonly required>
                            </div>
                            <label class="col-sm-1 col-form-label">Total Jual</label>
                            <div class="col-sm-3">
                                <input id="totaljual" name="totaljual" type="text" class="form-control" placeholder="Total Jual" readonly required>
                            </div>
                            <label class="col-sm-1 col-form-label" id="lpayment" style="display:none">Tenor</label>
                            <div class="col-sm-2">
                                <select id="payment" name="payment" class="form-control" style="display:none" required>
                                    <option style="display:none">Select Payment</option>
                                    <option value="c3">Cicilan 3x</option>
                                    <option value="c6">Cicilan 6x</option>
                                    <option value="c9">Cicilan 9x</option>
                                    <option value="c12">Cicilan 12x</option>
                                </select>
                            </div>
                        </div>
                        <div id="container2"></div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Form Inputs card end -->
        </div>
    </div>
@endsection

@section('css')
@endsection
@section('js')
<script>

    var i = 0;          
    $('#btntambah').on('click', function p1(){
        i++;
        var jmli = document.getElementById("jmli");
        jmli.value = i;
            var container = document.getElementById("container");
                
                var div1 = document.createElement('div');
                div1.classList.add("form-group");
                div1.classList.add("row");
                container.appendChild(div1);

                var label1 = document.createElement('label');
                label1.id = "lproduct" + i;
                label1.classList.add("col-sm-2");
                label1.classList.add("col-form-label");
                label1.style.fontWeight = "bold";
                label1.innerHTML = 'Product';
                div1.appendChild(label1);

                var col1 = document.createElement('div');
                col1.classList.add("col-sm-7");
                div1.appendChild(col1);

                var select = document.createElement("input");
                select.setAttribute("list", "dataproduct");
                select.id = "product" + i;
                select.name = "product" + i;
                select.classList.add("form-control");
                select.required=true;
                select.autocomplete="off";
                col1.appendChild(select);

                var datalist = document.createElement("datalist");
                datalist.id = "dataproduct";
                col1.appendChild(datalist);

                var data = {!! json_encode($product->toArray()) !!}
                data.forEach(f1)
                function f1(item) {
                    var option = document.createElement("option");
                    option.value = item.nama;
                    datalist.appendChild(option);
                }

                var col2 = document.createElement('div');
                col2.classList.add("col-sm-1");
                div1.appendChild(col2);

                var input1 = document.createElement("input");
                input1.type = "text";
                input1.id =  "jmlproduct" + i;
                input1.name = "jmlproduct" + i;
                input1.value = "1";
                input1.classList.add("form-control");
                input1.classList.add("text-center");
                input1.placeholder = "Jml";
                input1.required=true;
                col2.appendChild(input1);

                var colrm = document.createElement('div');
                colrm.classList.add("col-sm-2");
                div1.appendChild(colrm);

                var btnrm = document.createElement("button");
                btnrm.id = "btnrm" + i;
                btnrm.type = "button";
                btnrm.classList.add("btn");
                btnrm.classList.add("btn-primary");
                btnrm.classList.add("btn-md");
                btnrm.classList.add("btn-block");
                btnrm.classList.add("waves-effect");
                btnrm.classList.add("waves-light");
                btnrm.classList.add("text-center");
                btnrm.classList.add("m-b-20");
                btnrm.textContent = "Remove";
                colrm.appendChild(btnrm);

                var div2 = document.createElement('div');
                div2.classList.add("form-group");
                div2.classList.add("row");
                container.appendChild(div2);

                var label2 = document.createElement('label');
                label2.id = "lmodal" + i;
                label2.classList.add("col-sm-2");
                label2.classList.add("col-form-label");
                label2.innerHTML = 'Harga Modal';
                div2.appendChild(label2);

                var col2 = document.createElement('div');
                col2.classList.add("col-sm-4");
                div2.appendChild(col2);

                var input2 = document.createElement("input");
                input2.type = "text";
                input2.id =  "modal" + i;
                input2.name = "modal" + i;
                input2.classList.add("form-control");
                input2.placeholder = "Harga Modal";
                input2.required=true;
                col2.appendChild(input2);

                var label3 = document.createElement('label');
                label3.id = "ljual" + i;
                label3.classList.add("col-sm-1");
                label3.classList.add("col-form-label");
                label3.innerHTML = 'Harga Jual';
                div2.appendChild(label3);

                var col3 = document.createElement('div');
                col3.classList.add("col-sm-5");
                div2.appendChild(col3);

                var input3 = document.createElement("input");
                input3.type = "text";
                input3.id =  "jual" + i;
                input3.name = "jual" + i;
                input3.classList.add("form-control");
                input3.placeholder = "Harga Jual";
                input3.required=true;
                col3.appendChild(input3);
                
                $('#product'+i).on('input', function a1() { 
                        
                        var lpayment = document.getElementById("lpayment");
                        var payment = document.getElementById("payment");

                        lpayment.style.display = "block";
                        payment.style.display = "block";
                        
                        var id = this.id.replace('product','');
                        var jmlproduct = document.getElementById("jmlproduct"+id).value;
                        var modal = document.getElementById("modal"+id);
                        var jual = document.getElementById("jual"+id);
                        console.log(this);
                        var x = document.getElementById(this.id).value;
                        var tjual = document.getElementById("totaljual");
                        var tmodal = document.getElementById("totalmodal");
                        var data = {!! json_encode($product->toArray()) !!}
                        data.forEach(f1)
                        function f1(item) {
                            if(item['nama']==x){
                                modal.value = item['modal'] * jmlproduct;
                                jual.value = item['jual'] * jmlproduct;
                                var jjual = 0;
                                var jmodal = 0;
                                for(j=1;j<=i;j++){
                                    var jualan = document.getElementById("jual"+j);
                                    jjual += Number(jualan.value);
                                    var modalan = document.getElementById("modal"+j);
                                    jmodal += Number(modalan.value);
                                }
                                tmodal.value = jmodal;
                                tjual.value = jjual;
                            }
                        }
                    }
                );

                $('#jmlproduct'+i).on('input', function a2() {
                        var id = this.id.replace('jmlproduct','');
                        var jmlproduct = document.getElementById("jmlproduct"+id).value;
                        var modal = document.getElementById("modal"+id);
                        var jual = document.getElementById("jual"+id);
                        var x = document.getElementById("product"+id).value;
                        var tjual = document.getElementById("totaljual");
                        var tmodal = document.getElementById("totalmodal");
                        var data = {!! json_encode($product->toArray()) !!}
                        data.forEach(f1)
                        function f1(item) {
                            if(item['nama']==x){
                                modal.value = item['modal'] * jmlproduct;
                                jual.value = item['jual'] * jmlproduct;
                                var jjual = 0;
                                var jmodal = 0;
                                for(j=1;j<=i;j++){
                                    var jualan = document.getElementById("jual"+j);
                                    jjual += Number(jualan.value);
                                    var modalan = document.getElementById("modal"+j);
                                    jmodal += Number(modalan.value);
                                }
                                tmodal.value = jmodal;
                                tjual.value = jjual;
                            }
                        }
                        var tabimei = document.getElementById("tabimei"+id);
                        while (tabimei.hasChildNodes()) {
                            tabimei.removeChild(tabimei.lastChild);
                        }
                        for(a=1;a<=jmlproduct;a++){
                        
                        var label9 = document.createElement('label');
                        label9.id = "limei" + id + ","+ a;
                        label9.classList.add("col-sm-2");
                        label9.classList.add("col-form-label");
                        label9.innerHTML = 'Imei '+ a;
                        tabimei.appendChild(label9);

                        var col9 = document.createElement('div');
                        col9.classList.add("col-sm-2");
                        tabimei.appendChild(col9);

                        var select9 = document.createElement("input");
                        select9.id = "imei" + id + ","+ a;
                        select9.name = "imei" + id + ","+ a;
                        select9.classList.add("form-control");
                        select9.required= true;
                        col9.appendChild(select9);
                        }
                    }
                );

                $('#jual'+i).on('input', function a3() {
                    var total = document.getElementById("totaljual");
                    var jtotal = 0;
                    for(j=1;j<=i;j++){
                        var jualan = document.getElementById("jual"+j);
                        jtotal += Number(jualan.value);
                    }
                    total.value = jtotal;
                });
                $('#modal'+i).on('input', function a3() {
                    var total = document.getElementById("totalmodal");
                    var jtotal = 0;
                    for(j=1;j<=i;j++){
                        var jualan = document.getElementById("modal"+j);
                        jtotal += Number(jualan.value);
                    }
                    total.value = jtotal;
                });

                $('#btnrm'+i).on('click', function a4() {
                    var id = this.id.replace('btnrm','');
                    var jmlproduct = document.getElementById("jmlproduct"+id);
                    jmlproduct.value = "0"; 
                    jmlproduct.style.display = "none";
                    var modal = document.getElementById("modal"+id);
                    modal.value = "0";
                    modal.style.display = "none";
                    var jual = document.getElementById("jual"+id);
                    jual.value = "0";
                    jual.style.display = "none";
                    var x = document.getElementById("product"+id);
                    x.style.display = "none";
                    var lproduct = document.getElementById("lproduct"+id);
                    lproduct.style.display = "none";
                    var lmodal = document.getElementById("lmodal"+id);
                    lmodal.style.display = "none";
                    var ljual = document.getElementById("ljual"+id);
                    ljual.style.display = "none";
                    var btnrm = document.getElementById("btnrm"+id);
                    btnrm.style.display = "none";
                    
                    var totalm = document.getElementById("totalmodal");
                    var jtotalm = 0;
                    for(j=1;j<=i;j++){
                        var jualanm = document.getElementById("modal"+j);
                        jtotalm += Number(jualanm.value);
                    }
                    totalm.value = jtotalm;
                    var totalj = document.getElementById("totaljual");
                    var jtotalj = 0;
                    for(j=1;j<=i;j++){
                        var jualanj = document.getElementById("jual"+j);
                        jtotalj += Number(jualanj.value);
                    }
                    totalj.value = jtotalj;
                });

                var div9 = document.createElement('div');
                div9.id = "tabimei"+i;
                div9.classList.add("form-group");
                div9.classList.add("row");
                container.appendChild(div9);

                var label9 = document.createElement('label');
                label9.id = "limei" + i + ","+"1";
                label9.classList.add("col-sm-2");
                label9.classList.add("col-form-label");
                label9.innerHTML = 'Imei '+"1";
                div9.appendChild(label9);

                var col9 = document.createElement('div');
                col9.classList.add("col-sm-2");
                div9.appendChild(col9);

                var select9 = document.createElement("input");
                select9.id = "imei" + i+ ","+"1";
                select9.name = "imei" + i+ ","+"1";
                select9.classList.add("form-control");
                select9.required= true;
                col9.appendChild(select9);
        }
    );

    $('#payment').on('input', function p2(){
        var sales = document.getElementById("sales");
        var s = 0;
        if(sales.checked==true){
            s = 200000;
        }
        var totalj = document.getElementById("totaljual").value; 
        var payment = document.getElementById("payment").value;
        var dp = Number(totalj * 0.2 +s+199000);
        var cicilan = 0;
        var totalpayment = 0;
        if(payment=="c6"){
            cicilan = (Number(totalj) + (0.5 * Number(totalj))) / 6;
            totalpayment = (cicilan * 6)+dp;
        }else if(payment=="c9"){
            cicilan = (Number(totalj) + ((0.5 * Number(totalj)) + 500000)) /9;
            totalpayment = (cicilan * 9)+dp;
        }else if(payment=="c12"){
            cicilan = (Number(totalj) + ((0.5 * Number(totalj)) + 1000000)) /12;
            totalpayment = (cicilan * 12)+dp;
        }else{
            cicilan = 0;
            totalpayment = 0;
        }
        var container = document.getElementById("container2");
        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }

            var div1 = document.createElement('div');
            div1.classList.add("form-group");
            div1.classList.add("row");
            container.appendChild(div1);

            var label1 = document.createElement('label');
            label1.classList.add("col-sm-2");
            label1.classList.add("col-form-label");
            label1.innerHTML = 'Down Payment';
            div1.appendChild(label1);

            var col1 = document.createElement('div');
            col1.classList.add("col-sm-3");
            div1.appendChild(col1);

            var input3 = document.createElement("input");
            input3.type = "text";
            input3.id =  "dp";
            input3.name = "downpayment";
            input3.classList.add("form-control");
            input3.placeholder = "Down Payment";
            input3.value = dp;
            input3.required= true;
            col1.appendChild(input3);

            var label2 = document.createElement('label');
            label2.classList.add("col-sm-1");
            label2.classList.add("col-form-label");
            label2.innerHTML = 'Perbulan';
            div1.appendChild(label2);

            var col2 = document.createElement('div');
            col2.classList.add("col-sm-2");
            div1.appendChild(col2);

            
            var input4 = document.createElement("input");
            input4.type = "text";
            input4.id =  "perbulan";
            input4.name = "perbulan";
            input4.classList.add("form-control");
            input4.placeholder = "Perbulan";
            input4.value = cicilan;
            input4.required= true;
            col2.appendChild(input4);

            var label3 = document.createElement('label');
            label3.classList.add("col-sm-1");
            label3.classList.add("col-form-label");
            label3.innerHTML = 'Total';
            div1.appendChild(label3);

            var col3 = document.createElement('div');
            col3.classList.add("col-sm-3");
            div1.appendChild(col3);

            var input5 = document.createElement("input");
            input5.type = "text";
            input5.id =  "total";
            input5.name = "total";
            input5.classList.add("form-control");
            input5.placeholder = "Total";
            input5.value = totalpayment;
            input5.required= true;
            col3.appendChild(input5);
    });

    $('#sales').on('click', function p3(){
        var komisi = document.getElementById("komisi");
        var lkomisi = document.getElementById("lkomisi");
        var komisic = document.getElementById("komisic");
        var lkomisic = document.getElementById("lkomisic");
        var sales = document.getElementById("sales");
        var lmembername = document.getElementById("lmembername");
        var membername = document.getElementById("membername");
        var lmemberhp = document.getElementById("lmemberhp");
        var memberhp = document.getElementById("memberhp");
        if(sales.checked==true){
            komisi.value = 200000;
            komisi.style.display = "block";
            lkomisi.style.display = "block";
            komisic.value = 100000;
            komisic.style.display = "block";
            lkomisic.style.display = "block";
            lmembername.style.display = "block";
            membername.style.display = "block";
            lmemberhp.style.display = "block";
            memberhp.style.display = "block";
        }else{
            komisi.value = '';
            komisi.style.display = "none";
            lkomisi.style.display = "none";
            komisic.value = '';
            komisic.style.display = "none";
            lkomisic.style.display = "none";
            membername.value = '';
            lmembername.style.display = "none";
            membername.style.display = "none";
            memberhp.value = '';
            lmemberhp.style.display = "none";
            memberhp.style.display = "none";
        }
        
        
    });
</script>
@endsection