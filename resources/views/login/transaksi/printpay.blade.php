<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge" />
		<title>Invoice</title>
		<link
			href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap"
			rel="stylesheet"
		/>
		<style>
			@media print {
				@page {
					size: A3;
				}
			}
			ul {
				padding: 0;
				margin: 0 0 1rem 0;
				list-style: none;
			}
			body {
				font-family: "Inter", sans-serif;
				margin: 0;
			}
			table {
				width: 100%;
				border-collapse: collapse;
			}
			table,
			table th,
			table td {
				border: 1px solid silver;
			}
			table th,
			table td {
				text-align: right;
				padding: 8px;
			}
			h1,
			h4,
			p {
				margin: 0;
			}

			.container {
				padding: 20px 0;
				width: 1000px;
				max-width: 90%;
				margin: 0 auto;
			}

			.inv-title {
				padding: 10px;
				border: 1px solid silver;
				text-align: center;
				margin-bottom: 30px;
			}

			.inv-logo {
				width: 150px;
				display: block;
				margin: 0 auto;
				margin-bottom: 40px;
			}

			/* header */
			.inv-header {
				display: flex;
				margin-bottom: 20px;
			}
			.inv-header > :nth-child(1) {
				flex: 2;
			}
			.inv-header > :nth-child(2) {
				flex: 1;
			}
			.inv-header h2 {
				font-size: 20px;
				margin: 0 0 0.3rem 0;
			}
			.inv-header ul li {
				font-size: 15px;
				padding: 3px 0;
			}

			/* body */
			.inv-body table th,
			.inv-body table td {
				text-align: left;
			}
			.inv-body {
				margin-bottom: 30px;
			}

			/* footer */
			.inv-footer {
				display: flex;
				flex-direction: row;
			}
			.inv-footer > :nth-child(1) {
				flex: 2;
			}
			.inv-footer > :nth-child(2) {
				flex: 1;
			}
		</style>
	</head>
    @foreach($data as $d)
	<body>
		<div class="container">
			<div class="inv-title">
				<h1>Invoice # {{$d->kodetransaksi}}</h1>
			</div>
            <div style="height:60px; width:300px; background:#007bff">
									<img src="{{ asset('assets/images/logonew.png')}}" style="width: 300%; max-width: 300px" />
								</div>
			<div class="inv-header">
				<div>
					<h2>Markus Cell</h2>
					<ul>
						<li>Jl. Tiangseng No.11, RT.3/RW.6</li>
						<li>Jakarta</li>
						<li>0813-8100-1024</li>
					</ul>
					<h2>{{$d->username}} | {{$d->password}}</h2>
					<ul>
                        <li>Member : {{$d->membername}} | {{$d->memberhp}}</li>
						<li>{{$d->alamat}}</li>
						<li>{{$d->nohp}}</li>
					</ul>
				</div>
				<div>
					<table>
						<tr>
							<th>Date</th>
							<td>{{$d->tanggalbayar}}</td>
						</tr>
						<tr>
							<th>Tenor</th>
							<td>{{$d->jangkawaktu=='c3'?"3 Bulan":($d->jangkawaktu=='c6'?"6 Bulan":($d->jangkawaktu=='c9'?"9 Bulan":($d->jangkawaktu=='c12'?"12 Bulan":"")))}}</td>
						</tr>
						<tr>
							<th>Cicilan Perbulan</th>
							<td>Rp. {{number_format($d->cicilan)}}</td>
						</tr>
						<tr>
							<th>Status Pembayaran</th>
							<td>Lunas</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="inv-body">
				<table>
					<thead>
						<th>Cicilan Ke-</th>
						<th>Jatuh Tempo</th>
                        <th>Tanggal Bayar</th>
						<th>Harga Cicilan</th>
					</thead>
					<tbody>
						<tr>
							<td>
								<h4>{{\App\customclass\helpers::cicilanke($d->idTransaksi,$d->idPayment)}}</h4>
							</td>
                            <td>
								{{$d->jatuhtempo}}
                            </td>
                            <td>
								{{$d->tanggalbayar}}
							</td>
							<td>{{number_format($d->debitkredit)}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="inv-footer">
				<div><!-- required --></div>
				<div>
					<table>
						<tr>
							<th>Total Cicilan</th>
							<td>Rp. {{number_format($d->debitkredit)}}</td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>
	</body>
    @endforeach
</html>