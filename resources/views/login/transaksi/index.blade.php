@extends('layouts.app')

@section('content')
<div class="col-md-2">
    <a class="btn fa fa-plus-circle btn-primary btn-md btn-block waves-effect waves-light text-center m-b-10" href="{{route('newtransaksi')}}"><b> Tambah</b></a>
</div>
<div class="col-xl-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Data Transaksi</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table id="table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nomor Kontrak</th>
                            <th>Customer</th>
                            <th>Harga</th>
                            <th>Tanggal Transaksi</th>
                            <th>Status</th>
                            <th>Member</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data as $d)
                        <tr>
                            <th><center>{{$i++}}</center></th>
                            <td>{{$d->kodetransaksi}}</td>
                            <td>{{$d->nama}}</td>
                            <td>RP. {{number_format($d->total,2)}}</td>
                            <td>{{$d->created_at}}</td>
                            <td>{{\App\customclass\helpers::status($d->idTransaksi)}}</td>
                            <td>{{$d->membername}} | {{$d->memberhp}}</td>
                            <td><a href="{{route('printtransaksi',['id'=>$d->idTransaksi])}}">Print</a> | <a href="{{route('gettransaksi',['id'=>$d->idTransaksi])}}">View</a><td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@endsection
@section('js')
@endsection