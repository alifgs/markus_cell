@extends('layouts.app')

@section('content')
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Transaksi</h5>
                </div>
                @foreach($data as $d)
                <div class="card-block">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor Kontrak</label>
                            <div class="col-sm-3">
                                <input list="customer" name="customer" class="form-control" value="{{$d->kodetransaksi}}" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Customer</label>
                            <div class="col-sm-3">
                                <input list="customer" name="customer" class="form-control" value="{{$d->nama}}" >
                            </div>
                            <label class="col-sm-2 col-form-label">No Hp Customer</label>
                            <div class="col-sm-3">
                                <input list="customer" name="customer" class="form-control" value="{{$d->nohp}}" >
                            </div>
                            <div class="col-sm-1">
                                <a class="btn btn-primary" style="color: white" href="{{route('pay' ,['id'=>'$d->idPayment','mode'=>"pay"])}}">Chat</a>
                            </div>
                            
                        </div>
                        @if(!empty($d->membername))
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Member</label>
                            <div class="col-sm-3">
                                <input name="customer" class="form-control" value="{{$d->membername}}" >
                            </div>
                            <label class="col-sm-2 col-form-label">No Hp Member</label>
                            <div class="col-sm-3">
                                <input name="customer" class="form-control" value="{{$d->memberhp}}" >
                            </div>
                            <div class="col-sm-1">
                                <a class="btn btn-primary" style="color: white" href="{{route('pay' ,['id'=>'$d->idPayment','mode'=>"pay"])}}">Chat</a>
                            </div>
                        </div>
                        @endif
                </div>  
                <div class="card-block">      
                        @foreach(json_decode($d->idBarang) as $b)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label bold">Product</label>
                            <div class="col-sm-8">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="{{$b->product}}">        
                            </div>
                            <label class="col-sm-1 col-form-label">Jumlah</label>
                            <div class="col-sm-1">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="{{$b->jmlproduct}}">        
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Modal</label>
                            <div class="col-sm-4">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="Rp. {{number_format((int)$b->modal)}}">        
                            </div>
                            <label class="col-sm-2 col-form-label">Harga Jual</label>
                            <div class="col-sm-4">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="Rp. {{number_format((int)$b->jual)}}">        
                            </div>
                        </div>
                        @endforeach
                </div>
                <div class="card-block">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Total Modal</label>
                            <div class="col-sm-3">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="Rp. {{number_format((int)$d->modal)}}">
                            </div>
                            <label class="col-sm-1 col-form-label">Total Jual</label>
                            <div class="col-sm-3">
                                <input id="totaljual" name="totaljual" type="text" class="form-control" value="Rp. {{number_format((int)$d->jual)}}">
                            </div>
                            <label class="col-sm-1 col-form-label">Tenor</label>
                            <div class="col-sm-2">
                                <input id="totaljual" name="totaljual" type="text" class="form-control" value="{{$d->jangkawaktu=='c3'?"3 Bulan":($d->jangkawaktu=='c6'?"6 Bulan":($d->jangkawaktu=='c9'?"9 Bulan":($d->jangkawaktu=='c12'?"12 Bulan":"")))}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Down Payment</label>
                            <div class="col-sm-3">
                                <input id="totalmodal" name="totalmodal" type="text" class="form-control" value="Rp. {{number_format((int)$d->dp)}}">
                            </div>
                            <label class="col-sm-1 col-form-label">Perbulan</label>
                            <div class="col-sm-3">
                                <input id="totaljual" name="totaljual" type="text" class="form-control" value="Rp. {{number_format((int)$d->cicilan)}}">
                            </div>
                            <label class="col-sm-1 col-form-label">Total</label>
                            <div class="col-sm-2">
                                <input id="totaljual" name="totaljual" type="text" class="form-control" value="Rp. {{number_format((int)$d->total)}}">
                            </div>
                        </div>
                                <div class="col-md-12">
                                    <form action="{{route('transaksi')}}">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Back</button>
                                    </form>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Basic Form Inputs card end -->
        </div>
    </div>
@php($i=1)
@php($j=0)
@foreach($cicilan as $d)
<div class="col-sm-12">
    <div class="card">
        <div class="card-header" style="background-color: {{$d->status==0?'#ADD8E6':'#90EE90'}}">
            <div class="form-group row">
                <div class="col-sm-10">
                    <h5>Cicilan Ke-{{$i++}}</h5>
                </div>
                <div class="col-sm-2">
                    <h5>Tanggal : {{date("Y-m-d",strtotime($d->jatuhtempo))}}</h5>
                </div>
            </div>
        </div>
        <div class="card-block">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jumlah Cicilan</label>
                <div class="col-sm-2">
                    <label class="col-sm-12 col-form-label">Rp. {{number_format($d->debitkredit)}}</label>
                </div>
                <div class="col-sm-2">
                    @if(!empty($d->buktitransfer))
                        <div style="height:45px; width:50px; background:#007bff">
                            <a href="{{url('uploads/bukti/'.$d->buktitransfer)}}" target="_blank">
                            <img src="{{url('uploads/bukti/'.$d->buktitransfer)}}" style="width: 100%; max-width: 100px" />
                            </a>
                        </div>
                    @endif
                </div>
                <div class="col-sm-2">
                    @if(!empty($d->metode))
                            Metode bayar : {{$d->metode}}
                    @endif
                </div>
                @if(($d->status==0||$d->status==3)&&$j==0)
                @php($j=1)
                    <div class="col-sm-1">
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-primary" style="color: white" href="{{route('pay' ,['id'=>$d->idPayment,'mode'=>"pay"])}}">Lunas</a>
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-primary" style="color: white" href="{{route('pay' ,['id'=>$d->idPayment,'mode'=>"free"])}}">Gratis</a>
                    </div>
                @elseif($d->status==2||$d->status==3||$d->status==4)
                    <div class="col-sm-3">
                        <label class="col-sm-12 col-form-label">Tanggal Bayar : {{$d->tanggalbayar}}</label>
                    </div>
                @endif
                @if($d->status==2||$d->status==4)
                    <div class="col-sm-1">
                        <a class="btn btn-primary" style="color: white" href="{{route('printpay' ,['id'=>$d->idPayment])}}">Print</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div> 
@endforeach 
@endsection

@section('css')
@endsection
@section('js')
@endsection