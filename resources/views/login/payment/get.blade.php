@extends('layouts.app')

@section('content')
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Payment</h5>
                </div>
                <div class="card-block">
                        <input name="id" type="text" class="form-control" placeholder="Nama Product" value="{{$d->idTransaksi}}" hidden>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Product</label>
                            <div class="col-sm-10">
                                <input name="nama" type="text" class="form-control" placeholder="Nama Product" value="{{$d->nama}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Modal</label>
                            <div class="col-sm-10">
                                <input name="modal" type="number" class="form-control" placeholder="Harga Modal" value="{{$d->modal}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Jual</label>
                            <div class="col-sm-10">
                                <input name="jual" type="number" class="form-control" placeholder="Harga Jual" value="{{$d->jual}}" required>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Form Inputs card end -->
        </div>
    </div>
@endsection

@section('css')
@endsection
@section('js')
@endsection