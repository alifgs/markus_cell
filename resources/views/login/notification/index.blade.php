@extends('layouts.app')

@section('content')
<div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                <div class="card-header">
                    <h5>Notification Customer</h5>
                </div>
                <div class="card-block">
                    <form action="{{ route('postnotif') }}" method="post" class="md-float-material form-material" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Customer</label>
                            <div class="col-sm-10">
                                <input id="customernya" list="customer" name="customer" class="form-control" autocomplete="off" required >
                                <datalist id="customer">
                                    @foreach($pelanggan as $p)
                                        <option id="{{$p->idPelanggan}}" value="{{$p->nama}} | NIK : {{$p->noktp}}">
                                    @endforeach
                                    </select>
                                </datalist>
                                
                                <input id="idPelanggan" name="idPelanggan" hidden>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label id="lnohp" class="col-sm-2 col-form-label" style="display:none">No Hp</label>
                            <div class="col-sm-10">
                                <input id="nohp" name="nohp" style="display:none" class="form-control"placeholder="nohp" readonly>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Message</label>
                            <div class="col-sm-10">
                                <textarea id="message" name="message" class="form-control" rows="4" cols="50" required></textarea>
                            </div>
                        </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Send Notif</button>
                                </div>
                                </form>
                                <div class="col-md-12">
                                <form action="{{ route('postnotifwa') }}" method="post" class="md-float-material form-material" enctype="multipart/form-data">
                                @csrf
                                    <input id="hnohp" name="nohp" hidden class="form-control"placeholder="nohp">
                                    <input id="hmessage" name="message" hidden class="form-control"placeholder="message">
                                    <input id="hidPelanggan" name="idPelanggan" hidden>
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Send Whatsapp</button>
                                </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('url'))
        <?php $session_value=Session::get('urlnya')?>
    @endif
<div class="col-sm-12">
<!-- Basic Form Inputs card start -->
<div class="card">
    <div class="card-header">
        <h5>History Message</h5>
    </div>
    <div class="card-block">
    <div class="card-block table-border-style">
            <div class="table-responsive">
                <table id="table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Nama</th>
                            <th>Type</th>
                            <th>Message</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data as $d)
                        <tr>
                            <th><center>{{$i++}}</center></th>
                            <td>{{$d->nama}} | NIK : {{$d->noktp}} </td>
                            <td>{{$d->type}}</td>
                            <td>{{$d->message}}</td>
                            <td style="white-space:nowrap">{{$d->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@endsection
@section('js')
<script>
$('#customernya').on('change', function a1() { 
    var x = document.getElementById("customernya").value;
    var id = document.getElementById("idPelanggan");
    var hid = document.getElementById("hidPelanggan");
    var nohp = document.getElementById("nohp");
    var hnohp = document.getElementById("hnohp");
    var lnohp = document.getElementById("lnohp");
    var data = {!! json_encode($pelanggan->toArray()) !!}
    data.forEach(f1)
    function f1(item) {
        if((item['nama']+" | NIK : "+item['noktp'])==x){
            id.value = item['idPelanggan'];
            hid.value = item['idPelanggan'];
            nohp.style.display = "block";
            lnohp.style.display = "block";
            nohp.value = item['nohp'];
            hnohp.value = item['nohp'];
        }
    }
});

$('#message').on('input', function a2() {
    var message = document.getElementById("message");
    var hmessage = document.getElementById("hmessage");
    hmessage.value = message.value;
});
</script>
<script>
    var url= '<?php if(Session::has('urlnya')) echo $session_value;?>';
    
    window.onload = function(){
        if(url){
            window.open(url, "_blank");
        }
    }
</script>
@endsection