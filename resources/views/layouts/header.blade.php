<nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                           <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('home')}}">
                            <img class="img-fluid" src="{{ asset('assets/images/logonew.png')}}" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <i class="ti-bell"></i>
                                    @if(\App\customclass\helpers::notif()=='true')
                                        <span class="badge bg-c-red"></span>
                                    @endif
                                </a>
                                <ul class="show-notification">
                                    <li>
                                        <h6>Notifications</h6>
                                        @if(\App\customclass\helpers::notif()=='true')
                                            <label class="label label-danger">New</label>
                                        @endif
                                    </li>
                                    @foreach(\App\customclass\helpers::check3() as $d)
                                    <a href="{{route('gettransaksi',['id'=>$d->idTransaksi])}}">
                                    <li class="waves-effect waves-light">
                                        <div class="media">
                                            <img class="d-flex align-self-center img-radius" src="{{url('storage/uploads/bukti/'.$d->buktitransfer)}}" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <h5 class="notification-user">{{$d->nama}}</h5>
                                                <p class="notification-msg">{{$d->kodetransaksi}}</p>
                                                <p class="notification-msg">{{$d->metode}} Rp. {{$d->debitkredit}}</p>
                                                <span class="notification-time">{{\App\customclass\helpers::humanTiming($d->tanggalbayar)}}</span>
                                            </div>
                                        </div>
                                    </li>
                                    </a>
                                    @endforeach
                                    @foreach(\App\customclass\helpers::telat() as $d)
                                    <a href="{{route('gettransaksi',['id'=>$d->idTransaksi])}}">
                                    <li class="waves-effect waves-light">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="notification-user">{{$d->nama}} | Telat {{\App\customclass\helpers::remaining($d->jatuhtempo)}}</h5>
                                                <p class="notification-msg">{{$d->kodetransaksi}}</p>
                                                <p class="notification-msg">Jatuh Tempo {{date_format(date_create($d->jatuhtempo),"Y-m-d")}}</p>
                                                <p class="notification-msg">Rp. {{$d->debitkredit}}</p>
                                            </div>
                                        </div>
                                    </li>
                                    </a>
                                    @endforeach
                                    @foreach(\App\customclass\helpers::warning() as $d)
                                    <a href="{{route('gettransaksi',['id'=>$d->idTransaksi])}}">
                                    <li class="waves-effect waves-light">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="notification-user">{{$d->nama}} &nbsp; | Tempo {{\App\customclass\helpers::lebih($d->jatuhtempo)}}</h5>
                                                <p class="notification-msg">{{$d->kodetransaksi}}</p>
                                                <p class="notification-msg">Jatuh Tempo {{date_format(date_create($d->jatuhtempo),"Y-m-d")}}</p>
                                                <p class="notification-msg">Rp. {{$d->debitkredit}}</p>
                                            </div>
                                        </div>
                                    </li>
                                    </a>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <img src="{{ asset('assets/images/avatar-4.jpg')}}" class="img-radius" alt="User-Profile-Image">
                                    <span>{{Auth::user()->username}}</span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="#!">
                                            <i class="ti-settings"></i> Settings
                                        </a>
                                    </li>
                                    <li class="waves-effect waves-light">
                                        <a href="user-profile.html">
                                            <i class="ti-user"></i> Profile
                                        </a>
                                    </li>
                                    <li class="waves-effect waves-light">
                                        <a href="email-inbox.html">
                                            <i class="ti-email"></i> My Messages
                                        </a>
                                    </li>
                                    <li class="waves-effect waves-light">
                                        <a href="auth-lock-screen.html">
                                            <i class="ti-lock"></i> Lock Screen
                                        </a>
                                    </li>
                                    <li class="waves-effect waves-light">
                                        <a href="{{ route('logout') }}">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
