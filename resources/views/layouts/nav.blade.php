<nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                           
                            <div class="pcoded-navigation-label">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="{{strcmp('home',request()->segment(1))==0?'active':''}}" >
                                    <a href="{{route('home')}}" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                                        <span class="pcoded-mtext">Dashboard</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="{{strcmp('customer',request()->segment(1))==0?'active':''}}">
                                    <a href="{{route('customer')}}" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">Customer</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="{{strcmp('product',request()->segment(1))==0?'active':''}}">
                                    <a href="{{route('product')}}" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-mobile"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">Product</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="{{strcmp('transaksi',request()->segment(1))==0?'active':''}}">
                                    <a href="{{route('transaksi')}}" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">Transaction</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="{{strcmp('notification',request()->segment(1))==0?'active':''}}">
                                    <a href="{{route('notification')}}" class="waves-effect waves-dark">
                                        <span class="pcoded-micon"><i class="ti-bell"></i><b>BC</b></span>
                                        <span class="pcoded-mtext">Notification</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                    </nav>